function reset(){
	document.querySelector('#error-message').innerHTML = '';
	document.querySelector('h1').style.color = black;
	document.querySelectorAll('h2, .text-news').forEach(element => element.style.color = black);
}

function highlightSearchFind(elementToSearch, search){
	if(!elementToSearch.innerHTML.toLowerCase().includes(search.toLowerCase())) {
		return 0;
	}

	elementToSearch.style.color = blue;
	return 1;
}

function updateH1(search, isFind){
	let h1 = document.querySelector('h1');
	h1.innerHTML = search;

	if(!isFind)
		h1.style.color = red;
}

function afficherJSON(){
	let div = JSON.parse(allNewsJSON);
	logMessage(div);
}

function ajoutContentNews(news, allNews){
	let divGen = document.createElement('div');
	divGen.className = 'col-md-4'
	let newH2 = document.createElement('h2');
	let contentH2 = document.createTextNode(news.titre);
	let newP = document.createElement('p');
	let contentP = document.createTextNode(news.texte);
	let newBout = document.createElement('a');
	let contentBout = document.createTextNode('View detail');
	newBout.className = 'btn btn-secondary';
	newBout.href = '#';
	newBout.role= 'button'; 
	newH2.appendChild(contentH2);
	newP.appendChild(contentP);
	newBout.appendChild(contentBout);
	divGen.appendChild(newH2);
	divGen.appendChild(newP);
	divGen.appendChild(newBout);
	allNews.appendChild(divGen);
}

function ajoutNewsJSON(){
	let div = JSON.parse(allNewsJSON);
	let allNews = document.querySelector('#allNews');
	div.forEach(news => ajoutContentNews(news, allNews))
}