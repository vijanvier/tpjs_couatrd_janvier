function clickSearch(event){
	//permet de bloquer le submit d'un formulaire. Peut être remplacer un return false;
	event.preventDefault();
	reset();

	let inputSearch = document.querySelector('#search'); //sélecteur # permet de spécifier un ID
	if(inputSearch.value === '') {
		document.querySelector('#error-message').innerHTML = 'Recherche obligatoire !';
		return;
	}

	let elementsToSearch = document.querySelectorAll('.text-news'); //sélecteur . permet de spécifier une classe
	let occurence = 0;

	elementsToSearch.forEach(element => occurence += highlightSearchFind(element, inputSearch.value));
	updateH1(inputSearch.value, occurence > 0);
}

function clickDetail(currentBtn){
	let div = currentBtn.closest('div'); //permet d'obtenir le 1er ancètre (le parent) correspond au sélecteur passé en paramètre
	let text = div.querySelector('.text-news');

	logMessage(text.innerHTML);

	//permet de bloquer le submit d'un formulaire. Peut être remplacer event.preventDefault(); si event est en 1er paramètre
	return false;
}