//Utilisation du mot clé const pour la déclaration de constantes globales
const black = '#212529';
const blue = '#007bff';
const red = '#dc3545';
const green = '#28a745';

const allNewsJSON =
`
[
	{
		"id" : 1,
		"titre" : "News 1",
		"texte" : "C'est la news 1"
	},
	{
		"id" : 2,
		"titre" : "News 2",
		"texte" : "C'est la news 2"
	}, 
	{
		"id" : 3,
		"titre" : "News 3",
		"texte" : "C'est la news 3"
	}
]
`