function getHTMLNews(news) {
	let div = createHtmlElement('div', '', ['col-md-4']);
	let title = createHtmlElement('h2', news['title'], []);
	let desc = createHtmlElement('p', news['description'], ['text-news']);
	let a = createHtmlElement('a', 'View details »', ['btn', 'btn-secondary'], {'href': '#', 'role': 'button'});
	let pButton = document.createElement('p');

	pButton.append(a);
	div.append(title, desc, pButton); //permet de tout insérer en 1 seule instruction au lieu de faire plusieurs appendChild

	return div;
}