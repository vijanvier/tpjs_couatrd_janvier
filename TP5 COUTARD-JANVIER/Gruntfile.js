module.exports = function(grunt){
	grunt.initConfig({
		copy: {
		  main: {
		    files: [
		      // includes files within path
		      {expand: false, src: ['node_modules/bootstrap/dist/css/bootstrap.min.css'], dest: 'public/css/bootstrap.min.css', filter: 'isFile'},
		      {expand: false, src: ['node_modules/vue/dist/vue.min.js'], dest: 'public/js/vue.min.js', filter: 'isFile'},
		      {expand: false, src: ['node_modules/vue/dist/vue.js'], dest: 'public/js/vue.js', filter: 'isFile'},

		    ],
		  },
		},
		uglify: {
    		my_target: {
      			files: {
        			'public/js/app.min.js': ['src/js/const.js', 'src/js/log.js', 'src/js/content.js', 'src/js/events.js', 'src/js/news.js', 'src/js/main.js']
      			}
    		}
  		}
	});
	grunt.loadNpmTasks('grunt-contrib-copy');
	grunt.loadNpmTasks('grunt-contrib-uglify-es');
	grunt.registerTask('cp', ['copy']);
	grunt.registerTask('min',['uglify']);
};