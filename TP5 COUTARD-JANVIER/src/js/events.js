function clickSearch(event){
	//permet de bloquer le submit d'un formulaire. Peut être remplacer un return false;
	event.preventDefault();
	reset();

	let inputSearch = document.querySelector('#search'); //sélecteur # permet de spécifier un ID
	if(inputSearch.value === '') { //trim supprime les espaces
		document.querySelector('#error-message').innerHTML = 'Recherche obligatoire !';
		return;
	}

	let elementsToSearch = document.querySelectorAll('.text-news'); //sélecteur . permet de spécifier une classe
	let occurence = 0;

	elementsToSearch.forEach(element => occurence += highlightSearchFind(element, inputSearch.value));
	updateH1(inputSearch.value, occurence > 0);
}

function clickDetail(currentBtn){
	let div = currentBtn.closest('div'); //permet d'obtenir le 1er ancètre (le parent) correspond au sélecteur passé en paramètre
	let text = div.querySelector('.text-news');

	logMessage(text.innerHTML);

	//permet de bloquer le submit d'un formulaire. Peut être remplacer event.preventDefault(); si event est en 1er paramètre
	return false;
}

function clickAddNews(event){
	event.preventDefault();

	let titleNews = document.querySelector('[name="titleNews"]').value;
	let descNews = document.querySelector('[name="descNews"]').value;
	let nodes = document.querySelectorAll('.news');
	let idNews = parseInt(nodes[nodes.length- 1].id) + 1;
	let allNewsDiv = document.querySelector('#allNews');
	let news = {title: titleNews, description: descNews, ID: idNews};

	addNews(allNewsDiv, news);
}

function initEvents(){
	document.querySelector('button').onclick = clickSearch;
	document.querySelector('[name="addNewsBtn"]').onclick = clickAddNews;
	initEventNews();
}

function initEventNews() {
	let btnDetails = document.querySelectorAll('a');
	btnDetails.forEach(function(currentBtn){
		currentBtn.addEventListener('click', function(){
			clickDetail(currentBtn);
		});
	});
}