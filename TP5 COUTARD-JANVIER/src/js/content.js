function reset(){
	document.querySelector('#error-message').innerHTML = '';
	document.querySelector('h1').style.color = black;
	document.querySelectorAll('h2, .text-news').forEach(element => element.style.color = black);
}

function highlightSearchFind(elementToSearch, search){
	if(!elementToSearch.innerHTML.toLowerCase().includes(search.toLowerCase())) {
		return 0;
	}

	elementToSearch.style.color = blue;
	return 1;
}

function updateH1(search, isFind){
	let h1 = document.querySelector('h1');
	h1.innerHTML = search;

	if(!isFind)
		h1.style.color = red;
}

function createHtmlElement(tag, label, classList, attributs){	
	let element = document.createElement(tag);
	element.innerHTML = label;

	classList.forEach(function(item){
		element.classList.add(item);
	});

	if(attributs != undefined) {
		for (var key in attributs) {
			element.setAttribute(key, attributs[key]);
		}
	}

	return element;
}

function addNews(parent, news) {
	parent.appendChild(getHTMLNews(news));
}