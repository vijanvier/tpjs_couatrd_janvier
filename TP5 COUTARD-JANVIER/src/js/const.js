//Utilisation du mot clé const pour la déclaration de constantes globales
const black = '#212529';
const blue = '#007bff';
const red = '#dc3545';
const green = '#28a745';

const allNewsJSON = 
`[
	{"ID": 1, "title": "News 1", "description" : "Super news 1"},
	{"ID": 2, "title": "News 2", "description" : "Super news 2"},
	{"ID": 3, "title": "News 3", "description" : "Super news 3"}
]`;