function reset(){
	$('#error-message').html('');
	$('h1').css('color', black);
	$('h2, .text-news').each(function(){
		$(this).css('color', black);
	});
}

function highlightSearchFind(elementToSearch, search){
	if(!elementToSearch.html().toLowerCase().includes(search.toLowerCase())) {
		return 0;
	}

	elementToSearch.css('color', blue);
	return 1;
}

function updateH1(search, isFind){
	let h1 = $('h1');
	h1.html(search);

	if(!isFind)
		h1.css('color', red);
}

function createHtmlElement(tag, label, classList, attributs){	
	/*let element = document.createElement(tag);
	element.innerHTML = label;

	classList.forEach(function(item){
		element.classList.add(item);
	});

	if(attributs != undefined) {
		for (var key in attributs) {
			element.setAttribute(key, attributs[key]);
		}
	}

	return element;*/
	let element = $('<'+ tag +'></'+ tag +'>');
	element.html(label);
	classList.forEach(function(item){
		element.addClass(item);
	});

	if(attributs != undefined) {
		for (var key in attributs) {
			//logMessage(key);
			//	logMessage(attributs[key]);
			element.attr( {key : attributs[key]} );
		}
	}
	return element;
}

function addNews(parent, news) {
	//logMessage(news);
	parent.append(getHTMLNews(news));
}