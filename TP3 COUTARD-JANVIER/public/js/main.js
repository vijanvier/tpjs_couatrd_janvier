/* On attend que la page soit prête */
$( document ).ready(function() {
    /* On surveille les cliques sur les liens du menu */
    $('#tab-nav a').click(function(e){ 
        /* On désactive l'action par défaut des liens */
        e.preventDefault();
        /* On récupère la valeur de l'onglet à activer */
        var tab = $(this).data('tab');
        /* On masque tous les contenus */
        $('.tab').removeClass('tab-active');
        /* On affiche le contenu qui doit l'être */
        $('#'+tab).addClass('tab-active');
        /* On désactive tous les onglets */
        $('#tab-nav a').removeClass('tab-nav-active');
        /* On active l'onglet qui a été cliqué */
        $(this).addClass('tab-nav-active');
    });
});

let allNewsDiv = $('#allNews');
let newsJson = JSON.parse(allNewsJSON);
logMessage(newsJson);
newsJson.forEach(news => addNews(allNewsDiv, news));

var today = new Date(); 
var dd = String(today.getDate()).padStart(2, '0');
var mm = String(today.getMonth() + 1).padStart(2, '0');
var yyyy = today.getFullYear();
today = yyyy + '-' + mm + '-' + dd; 
var url = 'http://newsapi.org/v2/everything?q=minecraft&from='+ today +'&sortBy=publishedAt&apiKey=9b4e42c6404c48cab0f04cb200654d6c';
$.ajax({ url: url, method: "GET" })
.always(function(msg) {
 	logMessage(msg.articles)
 	msg.articles.forEach(msg => logMessage("Auteur : " + msg.author + "\nTitre : " + msg.title + "\nDescription : " + msg.description))
}
);



logHtmlElements('h2');
initEvents();