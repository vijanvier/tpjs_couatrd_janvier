function getHTMLNews(news) {
	let div = createHtmlElement('div', '', ['col-md-4']);
	let title = createHtmlElement('h2', news['title'], []);
	let desc = createHtmlElement('p', news['description'], ['text-news']);
	let a = createHtmlElement('a', 'View details »', ['btn', 'btn-secondary'], {'href': '#', 'role': 'button'});
	let pButton = $('<p></p>');
	pButton.append(a);
	div.append(title); 
	div.append(desc); 
	div.append(pButton);
	
	return div;
}

function getNewsForm() {

	let titre = $('#titreNewsForm').val();
	let desc = $('#descNewsForm').val();
	let allNewsDiv = $('#allNews');

	//logMessage(allNewsDiv.children());

	let news = new Object;
	news.ID = allNewsDiv.children().length + 1; 
	news.title = titre;
	news.description = desc;
	addNews(allNewsDiv, news)
	initEventNews();
}
