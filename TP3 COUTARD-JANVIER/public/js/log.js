function logMessage(message) {
	console.log(message);
}

function logHtmlElements(selector){
	let titles = $(selector);
	titles.each(function(){
		logMessage($(this).html());
	});
}