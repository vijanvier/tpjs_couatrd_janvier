function clickSearch(event){
	//permet de bloquer le submit d'un formulaire. Peut être remplacer un return false;
	event.preventDefault();
	reset();

	let inputSearch = $('#search'); //sélecteur # permet de spécifier un ID
	if(inputSearch.val() === '') { //trim supprime les espaces
		$('#error-message').html('Recherche obligatoire !');
		return;
	}

	let elementsToSearch = $('.text-news'); //sélecteur . permet de spécifier une classe
	let occurence = 0;

	elementsToSearch.each(function(){
		occurence += highlightSearchFind($(this), inputSearch.val());
	});
	updateH1(inputSearch.val(), occurence > 0);
}

function clickDetail(currentBtn){
	let div = currentBtn.parents('.col-md-4'); //permet d'obtenir le 1er ancètre (le parent) correspond au sélecteur passé en paramètre
	let text = div.children('.text-news');
	logMessage(text.html());

	//permet de bloquer le submit d'un formulaire. Peut être remplacer event.preventDefault(); si event est en 1er paramètre
	return false;
}

function initEvents(){
	$('button').click(function(event){
		clickSearch(event);
	});
	initEventNews();
}

function initEventNews() {
	let btnDetails = $('a');
	btnDetails.each((function(){
		$(this).click(function(){
		    clickDetail($(this));
		});
	}));
}

function validForm(){
	if (document.formNews.titre.value.length > 50) {
		logMessage("Le titre de la news doit faire moins de 50 carac !!");
		return false;
	}
	if(document.formNews.desc.value === ''){
		logMessage("La desc n'est pas remplie !!!");
		return false;

	}

	getNewsForm();
	return false;
}